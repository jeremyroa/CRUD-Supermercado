/**
 * @author Jeremy Roa
 * @date 10/12/2018
 * @description CRUD Supermercado
 */

require('./config/config')
const express = require('express')
const mongoose = require('mongoose');
const app = express()
const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({
    extended: false
}))

app.use(bodyParser.json())

app.use(require('./routes/index'))


app.use(express.static(__dirname + '/../public'))

mongoose.connect('mongodb://jeremy:123456a@ds042888.mlab.com:42888/super', {useNewUrlParser: true})
.then(() => console.log('Conectado a base de datos'))
.catch(err => console.log(err))

app.listen(process.env.PORT, () => console.log(`Escuchando puerto ${process.env.PORT}`))