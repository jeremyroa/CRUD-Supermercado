/**
 * @author Jeremy Roa
 * @date 10/12/2018
 * @description CRUD Supermercado
 */

const express = require('express');
const Cliente = require('../models/cliente');
const _ = require('underscore')
const app = express();
const validator = require('validator')

app.get('/cliente', (req, res) => {

    let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);
    if (isNaN(desde) || isNaN(limite)) {
        return res.status(400)
            .json({
                ok: false,
                message: 'No es un número el valor ingresado'
            })
    }
    Cliente.find({})
        .skip(desde)
        .limit(limite)
        .exec((err, clientes) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Cliente.count({}, (err, conteo) => {

                res.json({
                    ok: true,
                    clientes,
                    cuantos: conteo
                });

            });


        });

})
app.post('/cliente', (req, res) => {
    let body = req.body
    body.cedula = Number(body.cedula)
    body.edad = Number(body.edad)
    if (body.edad < 1 || isNaN(body.edad) || !validator.isBefore(body.fnacimiento) || body.cedula < 1 || isNaN(body.cedula)) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'Edad, fnacimiento y/o cedula incorrecta'
            }
        })
    }
    body.fnacimiento = validator.toDate(body.fnacimiento)

    let cliente = new Cliente({
        nombre: body.nombre,
        edad: body.edad,
        fnacimiento: body.fnacimiento,
        direccion: body.direccion,
        genero: body.genero,
        cedula: body.cedula
    })

    cliente.save((err, clienteDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            cliente: clienteDB
        });
    })

})
app.put('/cliente/:id', (req, res) => {

    let id = req.params.id;
    let body = _.pick(req.body, ['nombre', 'fnacimiento', 'edad', 'direccion', 'genero', 'cedula']);

    Cliente.findByIdAndUpdate(id, body, {
        new: true,
        runValidators: true
    }, (err, clienteDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }


        res.json({
            ok: true,
            cliente: clienteDB
        });

    })

})
app.delete('/cliente/:id', (req, res) => {
    let id = req.params.id
    Cliente.findByIdAndRemove(id, (err, clienteBorrar) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                error: err
            })
        }
        if (!clienteBorrar) {
            return res.status(400).json({
                ok: false,
                error: {
                    message: 'El Cliente ya se habia borrado antes'
                }
            })
        }

        res.json({
            ok: true,
            cliente: clienteBorrar
        })
    })
})

module.exports = app;