module.exports = {
    entry: './src/app/app.js',
    output: {
        path: __dirname + '/public',
        filename: 'pack.js'
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
              }
        }]
    }
}